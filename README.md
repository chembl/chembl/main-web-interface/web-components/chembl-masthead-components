# ChEMBL Masthead Components

This project contains the components used for the main structure of the masthead in the ChEMBL website.

# NPM Repository location 

https://www.npmjs.com/package/@chembl/chembl-masthead-components

**Note:** The configuration for the packaging and publishing of the components is in the 'library-packager' directory. See the gitlab-ci.yml file to see how the publishing is made. 

# Example Usage

You need to setup vuetify in your project before using the components.
An example of usage can be found here: 

https://codesandbox.io/s/chembl-component-usage-example-l8hur?file=/src/App.vue

# Development

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
